MODULE 1

## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module:

**Structured** [4]

* Is the structure understood at first glance (quick navigation)? yes
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)?  yes

(Comments)
Retire les templates mdr
**Complete** [4]

* Are the exercise objectives and checklist completed? yup
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided? yup

(Comments) 
Ouai j'ai l'impression, juste il manque ces points là fait gaffe : 
 documented how you are going to use project management principles
**Honest** [5]

* Are sources and inspirations properly referenced? Are the licenses used correctly? oui
* Is it clear what works, what doesn't work and what needs to be improved? oui

(Comments) 

**pedagogical/appropriate** \[5\]

* Based on the documentation, is the work reproducible if one has access to a fablab? oui 
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ? tout à fait

(Comments)

(general comment)

C'est top hein, juste retire les template et oublie pas de rajouter un petit point sur les methode de gestion de projet (genre comment tu fais pour faire ta doc à temps mdr)


MODULE 2

## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module: 2 

**Structured** [4]

* Is the structure understood at first glance (quick navigation)? yes
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)? ça manque un peu

(Comments)
Les images s'affichent pas, check la syntax : 
![](images/Module5Im/additionnalIdeas.jpg)

**Complete** [3]

* Are the exercise objectives and checklist completed?
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided?

(Comments)
En vrai, on voit pas trop ce que t'as fait (nottament à cause des images). Ca serait cool que tu rajoutes une petite explication de ton code et des images.
Oublie pas d'ajouter les code dans le folder file aussi (avec ta licence CC)
Il manque ces points là : 
- documented how you used other people's work and gave proper credit to complete your kit.
-  included the CC license to your work
- included your original design files
- shown how you did it with words/images screenshots
- documented how you used other people's work and gave proper credit to complete your kit.
**Honest** [5]

* Are sources and inspirations properly referenced? Are the licenses used correctly? yes
* Is it clear what works, what doesn't work and what needs to be improved? pas full mais oui

(Comments)

T'es honnête je pense haha

**pedagogical/appropriate** \[4]

* Based on the documentation, is the work reproducible if one has access to a fablab? quand t'auras mis les codes mais là c'est chaud ^^
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ? Belle plume et plein de liens et docs

(Comments)

(general comment)

Fait gaffe à ce module il se répète avec le 3 mais faut quand meme montrer ce que t'as fait qui à se répéter



MODULE 3 
## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module:

**Structured** [3]

* Is the structure understood at first glance (quick navigation)?
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)?

(Comments)
Je pense que tu pourrais rajouter des soustitres
+ RE probleme d'images
**Complete** [4]

* Are the exercise objectives and checklist completed? yes
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided? manque les files

(Comments)
Il manque juste les files sinon c'est complet

**Honest** [5]

* Are sources and inspirations properly referenced? Are the licenses used correctly?
* Is it clear what works, what doesn't work and what needs to be improved?

(Comments)
On ne peut plus honnête : "N'ayant pas tout compris directement[...]J'ai pas après appris qu'il y avait un (guide)"
**pedagogical/appropriate** \[5\]

* Based on the documentation, is the work reproducible if one has access to a fablab? si tu rajoute les files oui
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ?

(Comments)
Top l'explication des machines

(general comment)
Franchement bien l'explication de ta bataille avec la machine. Juste il manque les files , les photos s'affichent pas et ltu pourrais ajouter des soustitres selon moi




MODULE 4
## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module: 4

**Structured** [1-2-3-4-5]

* Is the structure understood at first glance (quick navigation)?
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)?

(Comments)

**Complete** [1-2-3-4-5]

* Are the exercise objectives and checklist completed?
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided?

(Comments)

**Honest** [5]

* Are sources and inspirations properly referenced? Are the licenses used correctly?
* Is it clear what works, what doesn't work and what needs to be improved?

(Comments)

**pedagogical/appropriate** \[1-2-3-4-5\]

* Based on the documentation, is the work reproducible if one has access to a fablab?
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ?

(Comments)

(general comment)

On fera une jolie maison en papier à la rentrée tkt


MODULE 5


## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module: 4

**Structured** [1-2-3-4-5]

* Is the structure understood at first glance (quick navigation)?
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)?

(Comments)

**Complete** [1-2-3-4-5]

* Are the exercise objectives and checklist completed?
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided?

(Comments)

**Honest** [5]

* Are sources and inspirations properly referenced? Are the licenses used correctly?
* Is it clear what works, what doesn't work and what needs to be improved?

(Comments)

**pedagogical/appropriate** \[1-2-3-4-5\]

* Based on the documentation, is the work reproducible if one has access to a fablab?
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ?

(Comments)

(general comment)

yet to be pushed
