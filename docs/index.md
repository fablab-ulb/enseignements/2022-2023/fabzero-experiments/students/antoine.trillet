## Foreword
This year, I have decided to follow this course in order to learn skills that are not taught in classical courses. I especially wanted to get confortable with the use of 3D printing and laser cutting machines as I intend to use these for future projects.  
I already had skills in elctronic and in project managment as I had courses and project in my previous years at ULB. But I beleive we can always learn more by doing new things with it in new contexts.  

In particular, my documentation brings togehter all thoughts and things I have learned through the project and the formations. I recommend anyone who struggles with some if it to skim through my modules and chapters as he or she might save some sweat.

## About me
I am a master 2 student in electronical engineering.  


I participated in a great project initiated by the CODEPO, the cooperation to devellopment organisation of the Ecole Polytechique de Bruxelles. We had the opportunity to go straight to Cambodia to implement pepper dryers that we had designed during our first year project. This was an incredible experience, here is a picture of all of us in the Cambodian university on the truck that would move our pepper dryers. I am in the red circle :)  
![](images\Cambodge.jpg)


I also have been a project manager for the BA1 students last year which was very instructive in term of team work analysis and whole project management. Here is the structure they have built  to collect energy from the movement of buildings: 

![](images\GreenVibes.jpg)









