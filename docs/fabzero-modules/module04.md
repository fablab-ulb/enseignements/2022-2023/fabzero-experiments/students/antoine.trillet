# 4. Laser cutters

The machine I have learned to use this week is the laser cutter. It can be used to cut and/or engrave 2D shapes into sheets of various materials. There are 3 machines with differents specs available at the FabLab which I will describe hereby but first let's remind one of the important safety gards while using the laser cutters. Most of the explanation on how to use the machines comes from this repository : [LASER Cutter Manual](https://gitlab.com/fablab-ulb/enseignements/fabzero/laser-cut)

## Safety gards
### Fire hazard
- Verify whether you can cut your material
- Activate the compressed air (execpt for epilog, it is automatic)
- Turn on the fume extractor
- Learn where the emergency STOP button is 
- Learn where the CO2 extinctor is
- Stay close to the machine until it has finished
- Remove residue after cutting has finished

### Health precaution 
 - Don't look at the LASER too intensely
 - Wear protection glasses
 - Do not open the machine if fumes are still inside


## Materials for cutting
*Always ask whether you can use a material that doesn't come from the FabLab*
### Recommanded materials
- Plywood / multiplex
- Acrylic (PMMA / Plexiglass)
- Paper, cardboard
- Textiles
### Not recommanded materials
*Only use in small quantity and of small thickness (max 3mm)*
- MDF: thick smoke, very harmful in the long term
- ABS, PS: melts easily, harmful smoke
- PE, PET, PP: melts easily
- Fiber composites: very harmful dust
- Metals: impossible to cut
### Prohibited materials

- PVC: acidic smoke, very harmful
- Copper: totally reflects the LASER
- Teflon (PTFE): acidic smoke, very harmful
- Phenolic resin, epoxy: very harmful smoke
- Vinyl, leatherette: may contain chlorine
- Animal leather: just because of the smell


## Process 
To **cut** matrial, you need an .svg (vectorial) file. Cutting requires power but for thicker materials, it is recommended to do more than one pass.    
To **engrave** materials, you need either:  
 - an .svg(vectorial) file with less power.  
 - Or a matricial file (.png, ...), the machine will then "print" each pixel

 To make your own .svg files, you can use InkScape : [InkScape](https://inkscape.org/fr/)
 ### Specify the power and speed
 On all 3 machines and programs of the fablab, to distinguish between the types of lines, either cut or engraved. We must inpaint them in colors and associate the color in the program with a specific speed (in mm/min) and power ( in W or % of maximum power).  
 
 Now to select the accurate speed and power for your application, you need to print a *calibration grid* first for you material and thickeness. Such a grid looks like this:  
 ![](images/Module4Im/board.jpg)
 If nothing is cutting through, you will need to some more than one pass. Pay attention to not moving the object between the passes ( this is why we first test on a calibration board)
## Epilog Fusion Pro 32 
Easy machine to use, autocalibration  included. 
 ![](images/Module4Im/epilog.jpg)
### Specifications
- Cutting area : 81 x 50 cm
- Maximum height: 31 cm
- LASER power : 60 W
- LASER type : CO2 tube (infrared)
- Camera on the tip
- Print SVG from Inkscape

### Manual
A step by step guide with the proposed settings for cutting and engraving with Inkscape and .SVG files: 
[Guide for the Epilog Fusion Pro](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)

## Lasersaur
Most powerfull machine made in FabLab but the software is on a raspberry py which is quite slow and it only accepts .svg. It is less automated.
 ![](images/Module4Im/lasersaur.jpg)
### Specifications

- Cutting area : 122 x 61 cm
- Maximum height: 12 cm
- Maximum speed: 6000 mm/min
- LASER power: 100 W
- Type of LASER: CO2 tube (infrared)
- Print .SVG only

### Manual
A step by step guide with the proposed settings for cutting and engraving with .svg : 
[Guide for the Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

[DriveBoard App](https://github.com/nortd/driveboardapp): control interface of the Lasersaur (already installed on the FabLab computers)


[Calibration grid](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/files/calibration_grid.dba) in .DBA format


## Full Spectrum Muse
Easiest but smallest machine, you can use your own laptop to control it. 
### Specififications
- Cutting area : 50 x 30 cm
- Maximum height: 6 cm
- LASER power: 40 W
- LASER type: CO2 tube (infrared) + red pointer
- SVG vectorial format
- BMP, JPEG, PNG, TIFF matricial format
- PDF matricial and/or vectorial format

### Manual
Retina Engrave, the control interface of the machine. 
To access it, connect your pc to the machine by ethernet cable.
If your laptop does not have an ethernet connection, use the dedicated wifi router.

Wifi : LaserCutter
CDM : fablabULB2019

Once the connection is established, enter the address [http://fsl.local](http://fsl.local) in your browser.

[Video tutorials](https://www.youtube.com/playlist?list=PL_1I1UNQ4oGa0w55C772Y1mC6F4f3ZcG6) to use Retina Engrave

[Settings Recommendations](https://fslaser.com/material-test/) from the Full Spectrum Laser site


## Cutting Our simple logo with the Lasersaur 

First a simple design was done on Adobe illustrator and exported to SVG format.  
Then we needed to change the color of the sides to enable cutting in red and engraving in black. 
Once imported in the lasersaur we selected the parameters from the calibration board for cardboard available at the FabLab. We went for [800 mm/min and 5 % of power] for the engraving and [600 mm/min and 20% of power] for cutting.   
After clicking on the two arrows next to run we realized the head went out of our carboard, so we moved it accordingly.  
Hereunder you can see the software with the svg file imported, we did .
 ![](images/Module4Im/software.jpg)

 Below is the final result: 
  ![](images/Module4Im/result.jpg)
*Note: We needed to reimport the svg file into inkscape and export in svg again as the colors were not recognised by the lasersaur software*

## Designing a kirigami drawer
### The .SVG file
![](images/Module4Im/mosaicDrawer.jpg)
This SVG file has been done on Inkscape and can be found in *files/module4*
- First I skteched aand dimensionned a drawer that can be folded into a 3D drawer (Pic1)
- Then I drew half of it using straight lines bezier curves and the rule visually(not very precise but it does not matter for this application)
- Then using the rotate horizontally and vertically option (Pic2) on a copy of the nodes, I got a symettrical shape Pic3
- Then I combined those with the Combine option (Pic3) into a single trace and added a handle
- Finally I added red lines to engrave lines to be folded

### Cutting and folding

Here is the result by cutting through cardboard: 
![](images/Module4Im/cardboardBox.jpg)

### Remarks on current desgin
- The bigger male attaches are quite big and hard to insert in the female holes: we should increase the wholes thickeness.
- The bending lines where cut with 10% and max speed, it cut though the first layer of carboard which eases the folding process a lot.
- too small drawer where very hard to assemble
- Assembling the left junction, the right one got out: it might help if there where unsymetrical junction clockwize 
