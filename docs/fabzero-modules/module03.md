# 3. 3D printing

This week was about learning how to 3D print and adadpt your sjketch so that it prints correctly.   

## My first 3D printed object
 As I missed time, I have printed a draft and smaller version of my handle without any other student contribution. 
 Then I have reworked my handle to use the flexilink from Morgan Tonglet. If I have some more time , I intend to reprint this new model full scale and with a better resolution.

## How to 3D print in the FabLab

First, export your design from freeCAD or openSCAD in .STL by cliking on *file* then *export*. An .STL file contains the geometry of your design
Now using PrusaSlicer, the software made for our 3D printers, we can create the .gcode file that tells the 3D printers how to print.   
For the rest , I refer you to the very neat explanation from the FabLab:
[FabLab tuturial for PRUSA 3D printers](
https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md)  

Recommended parameters (in easy mode): 
- 0.2 mm thick filament
- Generic PLA
- 15 % filling

*Advice*: check wether you have some material at the bottom of you design and if the details are still visible. Otherwise tweek the parameters to meet you needs.

### Some parameter choices to save printing time
- Take a thicker filament (0.30 mm) 
- Choose DRAFT mode
- Reduce the filling percentage to a reasonable amount (10%)
- Reduce the size of the design (here 70 %)
The details of the top and bottom of the pin are les precise but for the rest it seems fine
![](images/Module3Im/gcode.png)


## How it affected my design
Following some of the design rules from the lecture and this website:
[Prusa3D](https://help.prusa3d.com/article/modeling-with-3d-printing-in-mind_164135 ) 

The 3D model had to be slightly reconsidered to help the printing process.  
First thing is to **avoid overhangs**. I had an overhang at the outside top of the pin so i decided to remove it by adding a second cone in openSCAD to diminish the horizontality of the head of the pin. 

![](images/Module3Im/overhang.png)

## Result 
broken because to think + too discrete ( 3 big lines then 2, not smooth)
The pieces could easily be assembled, the pins bended but did not break as pressur was equally distributed on the pins.  
The margin taken between the different pieces is sufficient to disable friction but not too large.  
*However*, one of the pin has **broken** while i tried to disassemble them manually. **We should look into a solution if disassembling is a requirement**  
The failure happened close to a discretisation due to the thick filament i choose. This could be reduced by printing with smaller resolution. Also pressure should be equally distributed on the pin to not bend one too much. It would be better to press the handle on a **flat surface + make cones less large**.  
<img src="images/Module3Im/output2.gif" alt= “” width="200" height="value">     <img src="images/Module3Im/broken.jpg" alt= “” width="250" height="value"> 



## Add Morgan's contribution to my project

### The idea
Morgan Tonglet has built this flexilink which I intend to use to complement my 3D model:   [Morgan Tonglet's Gitlab](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/-/blob/main/docs/fabzero-modules/files/module2/mod2_flexilink001.scad)


My idea is as shown below, to add a flexilink between the moving parts so that they are brought back to an equilibrium state after being moved. A bit like a **spring**. 
![](images/Module3Im/sketch3.jpg)
For this purpose I will need to add two cylinder to each part so that I can fix Morgan's flexlink onto these by friction (tolerance must be kept low). Here is how I did it : 
![](images/Module3Im/flex.png)  

*Note: To test for different tolerances, I should iterate on the flexlink rather than the handle as it takes longer to print. A tolerance parameter allows us to easily test it:*
```
//Play with the tolerance between the two pieces
// it will change the flexlink holes

tolerance = 0.1; //µm
```
### Design parts parameters

- The **height** of the flexlink:  can be increased to sustain more flexion 
- The **curvature**of the flexlink: can be increased to reduce stress in the rest state of the flexlink
- The **tolerance** parameter: the bigger tolerance, the easier the elements will fit 
- The **thickness** of the flexlink : the thicker the more force to bring the elements back to rest

*Note*: while changing some parameters, especially the tolerance, the radius of the holes in the flexlink will increase. Therefore we will need to increase the width of the pins of the flexlink as it becomes to thin. 

## Printing
### First attempt
A  problem occured when printing everything with the extruder. It got stucked and nothing went out. 

Nevertheless, the gcodefile for printing is available in the file -> module3 folder. 
The handle had to be printed on its side with supports to print the buttons this time because there was a large floating part otherwize. 

The flex link is pretty great though.
### Second and third attempt

Same problem occured weeks later when i tried to print it again. I suspected the the temperature to be too high so i lowered it from 215 to 205 and it finally worked all the way. 

On the left is the unfinished one , and on the right is the final one. 

|       |      |
|--------------|-----------|
| ![](images/Module3Im/unfinishedflex.jpg) | ![](images/Module3Im/method1.jpg)      |




Finally I could print the whole thing. Below is an image of how it looks in standby when the flexlink is attached in one way or the other. Clearly the rest angle is better when the bending part is near the clipping one. 

|       |      |
|--------------|-----------|
| ![](images/Module3Im/finalflex.jpg) | ![](images/Module3Im/method2.jpg)      |



### Things that went wrong and improvement ideas

Here is a list of things that went wrong, some possible reasons and a proposition to eliminate these in the next iteration: 
- The **broken pins**: This time, i printed the design full scale (100 %). I thought thicker pins would be stronger, but they were even stiffer and broke directly. *Remedies: Reduce the arcs length so it is easier to bend towards the center + reduce the hat of the pin to reduce the maximum constrain.*

- The **flexlink does not clips**: It actually does clips well on the part that was printed lego pins upward, the radius is great. But it does not on the one printed on its side due to residual supports. *Remedies: bring a lime and remove some material, or use a slightly smaller radius these lego pins*

- the **rest angle isn't so great**: at rest there is almost no angle between the handles. *Remedy: use it the other way around like on the right picture above. Doing so we see that the flexlink bend weirdly, maybe consider making it a little shorter.*


I hope these reflections might help one who needs my design to fix these mistakes. For my part i will not repeat the process as it uses plastic for nothing. The device seems to work as expected, bending correctly, one should get something fully operational by following my proposed remedies above. 
