// File : handleAnimated.scad
// Author : Antoine Trillet
// Date : 27 february 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

$fn = 100;

//!!!!everything in cm when using factor of 10!!!!
sizeFactor = 10; 


//the thick handle (length, witdth, thickness)
hand = sizeFactor * [8, 3, 1.1];
joint = sizeFactor * 0.8;        //thickness of hand at the joint will influence thickness of handSupp
edgeRounding = sizeFactor * 0.2;
holeHand = sizeFactor * 1; 
holeXpos = sizeFactor * 1.5;


//The support and its bending clips attach
handSupp =  [hand[0] - holeXpos + hand[1]/2, hand[1], hand[2]];
jointSupp = hand[2]-joint;
spacer = sizeFactor * 0.1;


//cyclinder removal to let support rotate in handle
ExtrCySpace = sizeFactor * 0.2;
ExtrCyR = handSupp[1]/2*sqrt(2)+ExtrCySpace;
ExtrCyH = hand[2]-joint;


//cyclinder removal to let handle rotate in support
ExtrCyRSupp = sqrt(holeXpos^2 + (hand[1]/2)^2) + ExtrCySpace;
ExtrCyHSupp = jointSupp + joint ;

//Pins
PinThickness = sizeFactor * 0.1; 
PinH = joint + sizeFactor * 0.15;
PinOut = sizeFactor * 0.95;
PinIn = PinOut -PinThickness;
ConeOut = PinOut + sizeFactor * 0.15;
ConeH = sizeFactor * 0.15;
PinSpacer = sizeFactor * 0.5;
NbOfPin = 3;

module Handle(){
    difference(){
        minkowski()
        {
            translate([-(holeXpos-edgeRounding/2), -(hand[1]-edgeRounding)/2, edgeRounding/2])
            cube(hand-[edgeRounding, edgeRounding, edgeRounding]);
            sphere(edgeRounding/2);
        }
        cylinder(h = ExtrCyH, r = ExtrCyR);
        translate([-holeXpos-0.1,-hand[1]/2,0]) cube([holeXpos+0.1,hand[1], ExtrCyH]);
        cylinder (h = hand[2], r= holeHand);
    }
}



module AttachPins(){
    difference(){
        union(){
            cylinder(h = PinH, r = PinOut);
            translate([0,0,PinH])
            cylinder(ConeH,ConeOut, PinIn);
            translate([0,0,PinH - ConeH])
            cylinder(ConeH, PinIn, ConeOut);
            
        }
        minkowski(){
            translate([0,0, edgeRounding - 0.01])
            cylinder(2*PinH,r = PinIn-edgeRounding);
            sphere(edgeRounding);
        }
        for(i = [0 : NbOfPin-1]){
            rotate([0,0,360/NbOfPin*i])
            translate([0,-PinSpacer/2, -0.1 *  PinH]) 
            cube([2*ConeOut,PinSpacer, 2*PinH ]); 
        }
        
    }
    
}


module Support(){
    difference(){
        union(){
            minkowski()
            {
                rotate([0,0,90])
                translate([-hand[1]/2+edgeRounding/2, -(hand[1]-edgeRounding)/2, edgeRounding/2])
                cube([hand[1]+handSupp[1]+spacer, handSupp[1],handSupp[2]]-[edgeRounding, edgeRounding, edgeRounding]);
                sphere(edgeRounding/2);
            }
            minkowski()
            {
                translate([-handSupp[1]/2 + edgeRounding/2, hand[1]/2+edgeRounding/2+spacer, edgeRounding/2])
                cube(handSupp-[edgeRounding, edgeRounding, edgeRounding]);
                sphere(edgeRounding/2);
            }
        }
        
        translate([0,0,jointSupp])
        cylinder(h = ExtrCyHSupp, r = ExtrCyRSupp);
        
    }
    
}

module FullSupport(){
    union(){
        Support();
        translate( [0,0,jointSupp])
        AttachPins();
    }
}

/*if (0 <= $t && $t <0.2){
    translate([0,0,6*(0.2-$t)/0.2])
    Handle();
    color([0.0,0.5,0.5]) FullSupport();
}

else if (0.2 <= $t && $t < 0.6){
    Handle();
    color([0.0,0.5,0.5]) rotate([0,0,($t-0.2)*180/0.4]) FullSupport();
}
else if (0.6 <= $t && $t< 1){
    Handle();
    color([0.0,0.5,0.5]) rotate([0,0,180-180/0.4*($t-0.6)]) FullSupport();
}*/
//else{
    FullSupport();
//}




















