// File : boiteRain.scad
// Author : Antoine Trillet
// Date : 25/04/2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)




$fn = 50;


W = 140; //y
Lbox = 80; //x
Lglass = 80; //x
H = 60; //z
angle = 30; //glass angle
thickness = 5; 
edges = 3;

separation = 2;
//dimensions of the screw support
Hscrew  = 12;
Rscrew = 12;
HoleScrew = 1.5; //radius for the screw itself


module attachWire(){
    radius = 3;
    height = 2;
    difference(){
        cylinder(h= height, r = radius);
        translate([0,0,-0.1]) cube(radius+1);
        translate([0,-radius-1+0.1,-0.1]) cube(radius + 1);
        translate([0,0,-0.1]) cylinder(h= 3, r = radius-1);
    }
}
module screwSupport(H,R,Rin,edgeS){
    intersection(){
        difference(){
            cylinder(h = H, r1 = R, r2 = 0);
            translate([edges+Rin,edges+Rin,-0.1]) cylinder(h = H, r = Rin);   
        }
        cube(2*R);
    }
}
module supportEdges(Le,We,He,edgeE,thickE){
    CubePoints = [
  [  edgeE,  edgeE,  -0.1 ],  //0
  [ Le-edgeE,  edgeE,  -0.1 ],  //1
  [ Le-edgeE,  We-edgeE,  -0.1 ],  //2
  [  edgeE,  We-edgeE,  -0.1 ],  //3
  [  0,  0,  He+0.1 ],  //4
  [ Le,  0,  He+0.1 ],  //5
  [ Le,  We,  He+0.1 ],  //6
  [  0,  We,  He+0.1 ]]; //7
  
    CubeFaces = [
  [0,1,2,3],  // bottom
  [4,5,1,0],  // front
  [7,6,5,4],  // top
  [5,6,2,1],  // right
  [6,7,3,2],  // back
  [7,4,0,3]]; // left
  union(){
    difference(){
        cube([Le,We,He]);
        //translate([edgeE,edgeE,-He/2]) cube([Le-2*edgeE,We-2*edgeE,2*He]);
    //cube([Le-2*edgeE,-2*edgeE,He]);
        polyhedron(CubePoints, CubeFaces );
    }
    //add the holes for the screw
    translate([0,0,-thickE/2]) rotate([0,0,0]) screwSupport(Hscrew, Rscrew, HoleScrew, edgeE);
    translate([Le,0,-thickE/2]) rotate([0,0,90]) screwSupport(Hscrew, Rscrew, HoleScrew, edgeE);
    translate([Le,We,-thickE/2]) rotate([0,0,180]) screwSupport(Hscrew, Rscrew, HoleScrew, edgeE);
    translate([0,We,-thickE/2]) rotate([0,0,270]) screwSupport(Hscrew, Rscrew, HoleScrew, edgeE);
}
}


module Mainbox(){
    difference(){
        union() {
            difference(){
                minkowski(){
                    //bias for glass
                    difference(){
                        cube([Lbox+Lglass,W,H]);
                        translate([Lbox,0,H]) rotate([0,angle,0]) cube(W);
                    }
                    sphere(thickness);
                }
                //remove same shape to have hollow shape
                difference(){
                    cube([Lbox+Lglass,W,H]);
                    translate([Lbox,0,H]) rotate([0,angle,0]) cube(W);
                }
                
            }
            //wall inside
            translate([Lbox-thickness/4,0,0]) cube([thickness,W,H]);
        }
        //edges glass
        translate([Lbox,0,H-0.1]) rotate([0,angle,0]) translate([0,0,thickness/2]) cube([Lglass/cos(angle),W, 2*thickness]);
        translate([Lbox,0,H-0.1]) rotate([0,angle,0]) translate([2*edges,2*edges,0]) cube([Lglass/cos(angle)-2*edges- 2*edges,W-2*2*edges, 2*thickness]);
        //edges mainbox
        translate([0,0,-0.1-thickness]) cube([Lbox-separation/2,W, thickness/2+0.1]);
        translate([edges,edges,thickness/2-0.1-thickness]) cube([Lbox - 2*edges-separation/2,W-2*edges, 2*thickness]);
        //edges under trap
        translate([Lglass+separation/2+thickness*1/4,0,-0.1-thickness]) cube([Lglass-separation/2-thickness*1/4,W, thickness/2+0.1]);
        translate([Lglass+separation/2+1/4*thickness+edges,edges,thickness/2-0.1-thickness]) cube([Lglass - 2*edges-1/4*thickness-separation/2,W-2*edges, 2*thickness]);
        //hole to pass wire
        translate([Lbox-thickness/4-0.1,3/4*W,3/4*H])rotate([0,90,0]) cylinder(h = 2*thickness, r = 6);
    }
   
    // ring for wires
    translate([Lbox + 3/4*thickness,3/4*W,1/2*H]) rotate([90,0,0])rotate([0,0,180]) attachWire();
    // support for the edges
    supportEdges(Lbox-thickness/4,W,2,edges,thickness);
    translate([Lbox+separation,0,0]) supportEdges(Lglass-separation/2-thickness*1/4,W ,2,edges,thickness);
}



Mainbox();