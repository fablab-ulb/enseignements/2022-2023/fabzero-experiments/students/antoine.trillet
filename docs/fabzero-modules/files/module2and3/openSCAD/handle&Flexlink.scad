// File : handleAnimated.scad
// Author : Antoine Trillet
// Date : 27 february 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)


//This project uses some code from Morgan Tonglet : https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/-/blob/main/docs/fabzero-modules/files/module2/mod2_flexilink001.scad

//Play with the tolerance between the two pieces
// it will change the flexlink holes

tolerance = 0.1; //µm



$fn = 50;

//!!!!everything in cm when using factor of 10!!!!
sizeFactor = 10; 


//the thick handle (length, witdth, thickness)
hand = sizeFactor * [8, 3, 1.1];
joint = sizeFactor * 0.8;        //thickness of hand at the joint will influence thickness of handSupp
edgeRounding = sizeFactor * 0.2;
holeHand = sizeFactor * 1; 
holeXpos = sizeFactor * 1.5;


//The support and its bending clips attach
handSupp =  [hand[0] - holeXpos + hand[1]/2, hand[1], hand[2]];
jointSupp = hand[2]-joint;
spacer = sizeFactor * 0.1;


//cyclinder removal to let support rotate in handle
ExtrCySpace = sizeFactor * 0.2;
ExtrCyR = handSupp[1]/2*sqrt(2)+ExtrCySpace;
ExtrCyH = hand[2]-joint;


//cyclinder removal to let handle rotate in support
ExtrCyRSupp = sqrt(holeXpos^2 + (hand[1]/2)^2) + ExtrCySpace;
ExtrCyHSupp = jointSupp + joint ;

//Pins
PinThickness = sizeFactor * 0.1; 
PinH = joint + sizeFactor * 0.15;
PinOut = sizeFactor * 0.95;
PinIn = PinOut -PinThickness;
ConeOut = PinOut + sizeFactor * 0.15;
ConeH = sizeFactor * 0.15;
PinSpacer = sizeFactor * 0.5;
NbOfPin = 3;


//Flexlink params
//Constants
e=0.05;

//Parameters [mm]
h=5;
rad_in=4 + tolerance;
rad_ext=6;
rad_arc=50;
n_holes=3;
holes_gap=0;
thick=1;

//cylinder to attach the flexlink


cyH = 4;  //height 
cyR = rad_in - tolerance;  //radius
cySpacing = 2 * rad_ext;
cyXpos = handSupp[1]; //x pos of left most pin


module Handle(){
    difference(){
        union(){
            minkowski()
            {
                translate([-(holeXpos-edgeRounding/2), -(hand[1]-edgeRounding)/2, edgeRounding/2])
                cube(hand-[edgeRounding, edgeRounding, edgeRounding]);
                sphere(edgeRounding/2);
            }
            translate([cyXpos,0,hand[2]])
            cylinder(h=cyH, r = cyR);
            translate([cyXpos + cySpacing,0, hand[2]])
            cylinder(h=cyH, r = cyR);
            translate([cyXpos + 2 * cySpacing,0, hand[2]])
            cylinder(h=cyH, r = cyR);
        }
        cylinder(h = ExtrCyH, r = ExtrCyR);
        translate([-holeXpos-0.1,-hand[1]/2,0]) cube([holeXpos+0.1,hand[1], ExtrCyH]);
        cylinder (h = hand[2], r= holeHand);
        
    }
}



module AttachPins(){
    difference(){
        union(){
            cylinder(h = PinH, r = PinOut);
            translate([0,0,PinH])
            cylinder(ConeH,ConeOut, PinIn);
            translate([0,0,PinH - ConeH])
            cylinder(ConeH, PinIn, ConeOut);
            
        }
        minkowski(){
            translate([0,0, edgeRounding - 0.01])
            cylinder(2*PinH,r = PinIn-edgeRounding);
            sphere(edgeRounding);
        }
        for(i = [0 : NbOfPin-1]){
            rotate([0,0,360/NbOfPin*i])
            translate([0,-PinSpacer/2, -0.1 *  PinH]) 
            cube([2*ConeOut,PinSpacer, 2*PinH ]); 
        }
        
    }
    
}


module Support(){
    difference(){
        union(){
            minkowski()
            {
                rotate([0,0,90])
                translate([-hand[1]/2+edgeRounding/2, -(hand[1]-edgeRounding)/2, edgeRounding/2])
                cube([hand[1]+handSupp[1]+spacer, handSupp[1],handSupp[2]]-[edgeRounding, edgeRounding, edgeRounding]);
                sphere(edgeRounding/2);
            }
            minkowski()
            {
                translate([-handSupp[1]/2 + edgeRounding/2, hand[1]/2+edgeRounding/2+spacer, edgeRounding/2])
                cube(handSupp-[edgeRounding, edgeRounding, edgeRounding]);
                sphere(edgeRounding/2);
            }
           
        }
        
        translate([0,0,jointSupp])
        cylinder(h = ExtrCyHSupp, r = ExtrCyRSupp);
        
    }
    
}

module FullSupport(){
    union(){
        Support();
        translate( [0,0,jointSupp])
        AttachPins();
        translate([cyXpos,hand[1]/2 + spacer + handSupp[1]/2 ,hand[2]])
        cylinder(h=cyH, r = cyR);
        translate([cyXpos + cySpacing, hand[1]/2 + spacer + handSupp[1]/2, hand[2]])
        cylinder(h=cyH, r = cyR);
        translate([cyXpos + 2 * cySpacing, hand[1]/2 + spacer + handSupp[1]/2, hand[2]])
        cylinder(h=cyH, r = cyR);
    } 
}


// MORGAN TONGLET PART 
// File : mod2_flexilink001.scad
// Author : Morgan Tonglet
// Date : 26 février 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)


//Functions    
module part(n_holes,h,r_in,r_ext,gap){
  difference(){
    //drawing the external part
    hull(){
        cylinder(h,r=r_ext);
        translate([0,(n_holes-1)*(2*r_ext+gap),0])
        cylinder(h,r=r_ext);
    };
    //removing material for holes
    union(){
      for(i=[0:n_holes-1]){
        translate([0,i*(2*r_ext+gap),-0.1])
        cylinder(h+0.2,r=r_in);
      }
    }
  }
}

module arc(thickness, h, radius){
  t=thickness;
  d=(rad_ext-rad_in)/2;
  difference(){
    cylinder(h=h,r=radius+t/2);
      translate([0,0,-e])
      union(){
        cylinder(h=h+2*e,r=radius-t/2);
        translate([d,d,0])
        cube([rad_arc+t,rad_arc+t,h+2*e]);
        translate([-rad_arc-t,0,0])
        cube([rad_arc+t,rad_arc+t,h+2*e]);
        translate([-rad_arc-t,0,0])
        cube([rad_arc+2*t,rad_arc+2*t,h+2*e]);
        translate([-rad_arc-t-d,-rad_arc-t,0])
        cube([rad_arc+t,rad_arc+2*t,h+2*e]);
      }
  }
}
module FlexLink(){
    union(){
    translate([0,rad_ext,0])//not rad_in
    part(n_holes,h,rad_in,rad_ext,holes_gap);
    translate([-rad_ext-rad_arc,-rad_arc,0])
    rotate([0,0,90])
    part(n_holes,h,rad_in,rad_ext,holes_gap);
    translate([-rad_arc,0,0])
    arc(thick,h,rad_arc);
    }
}
    


/*if (0 <= $t && $t <0.2){
    translate([0,0,6*(0.2-$t)/0.2])
    Handle();
    color([0.0,0.5,0.5]) FullSupport();
}

else if (0.2 <= $t && $t < 0.6){
    Handle();
    color([0.0,0.5,0.5]) rotate([0,0,($t-0.2)*180/0.4]) FullSupport();
}
else if (0.6 <= $t && $t< 1){
    Handle();
    color([0.0,0.5,0.5]) rotate([0,0,180-180/0.4*($t-0.6)]) FullSupport();
}*/
//else{
    translate([0.01,0.01,0.01])
    Handle();
    color([0.0,0.5,0.5]) FullSupport();
    translate([120,0,0]) FlexLink() ; 
//}


















