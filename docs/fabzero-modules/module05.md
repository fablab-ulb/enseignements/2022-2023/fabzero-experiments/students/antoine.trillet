# 5. Group dynamic and final project


## Project analysis and design
In this lecture we have learned how to analyse a problem and come up with solutions. This will be especially useful for the rest of the course as we will identify problematics and solution we could implement as our group project.  

## The problem and solution tree method

The proposed method to pose the problem and identify its roots and branches is the **problem and solution tree**. Its functionnement is very well explained by the european commission in this video: [Tree problem](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY).  
In short, we have to build a tree whose: 
- *Trunk* is the problem we want to address
- *Roots* are the causes
- *Branches* are the consequences  

And transform this tree into a soltion tree by changing the: 
- *Trunk* from problem to the objective
- *Roots* from cause to solution
- *Branches* from consequences to outcomes

## My problem and solution tree
I decided to do the exercice myself on a problematic that matters to me which is the overuse of dry magnesie in climbing gyms which is toxic for lungs. 

### My problem tree diagram
The problem is precisely the fact that dry magnesie particles flow in the air when people use it and accumulate in our lungs.  
I pointed out some potential causes to this problem, from the loose bags to the mental addiction to magnesie in climbing communities. And described some of the consequences I know. 
![](images/Module5Im/TreeProblem.jpg)

### My solution tree diagram
From there it was surprisingly easy to transform my causes and consequences into solutions and outcomes. I beleive I unconsciously had built my causes into possibly solutionnable causes. And it was just a question of perspective to uncover the solutions.
![](images/Module5Im/TreeSolution.jpg)

## Method to create groups, initiate thematics and problematics
- Today, we all brought an object that remind us of a problematic that matters
- Then we create groups based on the links between the object we had. This was very natural through discussion with other students
- Together, we wrote down links between our objects in 3min
- From these, we built thematics that matters to us all and ranked them with a non judging system shown below
- Finally, we determined what problematics came out of these thematics for each of us. Then discarded the not interresting ones. We also had time to ask other groups what problematic they would suggest based on our thematics.

## My object 

Today I came with a piece of plant that dried last summer of lack of water.  
This inpired me the difficulty of adjusting the amount of water to use for plant in dry and hot seasons. Furthemore, it also reminded me of the serious drought I was confronted with in South Africa in 2018 which led to ingenious ways of saving water. It still beleive there remains many challenges to overcome to improve our society's resilience to water shortages.

![](images/Module5Im/Branch.jpg)

## My group
We are four students in my group. Halil, physicist, Martin, bioengineer, Eliot, physic engineer and myself, electronics engineer.  
We all brought objects that could be linked throught the *nature* attribute: 2 rocks, 1 dryed branch and 1 dried plant.   

These unformal brainstorming methods unexcectedly helped binding our group very quickly. Here is a group picture of our group just 2 hours after we met: 
![](images/Module5Im/Group.jpg)


## Our thematics and problematics
We have found 4 thematics we could exploit further, ranked from more to less inspiring: 

- Climbing 
- Water 
- Reusability and low tech
- Ressources sparcity  

These were selected out of the ideas that linked our objects, we had to find in 3 min:  
![](images/Module5Im/BigSheet.jpg)
 

Below is a list of problematics that came out of the problematic generation process: 
![](images/Module5Im/Problematics.jpg)

And here is the list of ideas I collected while talking to the other groups: 
![](images/Module5Im/additionnalIdeas.jpg)


*Note: It was very interresting to see how we were drifting towards climbing as it was a thematic that linked us all even though it had nothing to do with the objects we had brought in. I think it promises a lot in terms of future individual investment as we have gone for something that matters for us rather than a thematic that might matters more overal but inpires us less.*


## Group Dynamics tools

We have had a session to discover and try some dynamics tools that might help us later for the project.  
Hereunder I am going to tell your about 3 handtechniques to comunicate and take decisions quickly without having to talk 

### Why hand techniques are great
Using your fingers to show a number or wing it for approval is quite useful in most cases to reduce interruptions and to accelerate decision making. Indeed it requires no tool nor talking which might take long and risk sliding into endless discussions. 

- Use it when you feel like approving or disaproving without interrupting
- Use it to quickly get the heat about an idea
- Use it to reduce the talking for things that don't matter too much


### Talking ordered with fingers on the table

*When* the group is no too big so you need somoene who shares the talking but too big to let everyone express him/herself without interrupting.  The talking is ordered collectively with ease.

*How* : Place your finger on the table when you want to add something after the one talking. The next person to do so places 2 fingers (he/she is the second to talk). 


### Getting the heat/approval by waving hands 
*When* a decision can be taken quickly or we want check the heat about something. 

*How* : Soemoene tells when to wave the hand.
- Waving towards the ground: disaprove
- Waving towards the ceiling: approve
- Waving horizontally/diagonaly : between approval and disaproval, no clear opinion


This trick can also be used to approve people when they are talking. This is especially useful when many people listen as the speaker knows he is not alone thinking that and is approved to continue. 

 ### Taking decisions by showing a number simultaneously

*When* a choice has to be taken between proposisitons or tasks must be allocated. It also brings more nuance than the previous technique.

*How*: Everyone shows a number from 0 to 5 for each proposition. The proposition with the most points is the prefered one. If we are splitting tasks the one with the most fingers takes the task. 
