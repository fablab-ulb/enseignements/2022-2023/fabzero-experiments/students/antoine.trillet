# 2. Conception Assistée par Ordinateur (CAO)

This week's goal is to familiarize with new open source CAD tools for 2D and 3D modeling (inkscape, openSCAD, FreeCAD) and model a tool or toy that uses compliant mechanics (this refers to the ability of our object to deform in a wishful way).

## The project Idea

### What I wanted to do
At first, I spent some time imagining a way to inject velocity to an object via a complied piece of plastic. I came out with this sketch that resemble a gun in some way, though it was not intended initially.  
Pressing on the handle would bring the ... to a blocking piece. It would bend until it slips below and releases its energy in a projectile well positionned. Ideally the handle could come back to its open state using a spring or a bended plastic stic.  

![](images/Module2Im/sketchHand.jpg)
### What I acheived

After some work on the handle itself, I realised i could use some compliant mechanics to clips the two pieces together. This was a nice project in itself and I made it neat and parametric to let anyone use it. 

The idea behind the bending pins it to allow one to change one of the handle without reprinting everything. Otherwise we could have 3D printed one into the other. 
If I have more time to spend on it, I will add the missing parts. In the mean time, let's discuss here how I modeled such clipping handle and what difficulties I have encountered

![](images/Module2Im/animatedGIF.gif)
## Using openSCAD to design the Handle

### Why openSCAD
The main reason is probably its simplicity as a first CAD tool but it is also very convinient to share it as it it text based.  
Small but complete cheat sheet : [OpenScad Cheat Sheet](https://openscad.org/cheatsheet/)   
The teacher's gitlab for more: [Teacher's tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad)  

You can download my .scad  and freeCAD objets here: [Module 2-3 files](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/-/tree/main/docs/fabzero-modules/files)

*Note: these have no biased edge to help fitting the pieces. If you need to modify the dimensions of the .scad, don't forget to rework the edges by following the freeCAD tutorial below*




### Tricks and examples

- *Precise which parameters you are using*  
For a cylinder you need to preceise h, r1 and r2, but you can also write it like that, where r1=r2=r: 
```
cylinder(h = ExtrCyH, r = ExtrCyR);
```
- *The order of translation,... matters !*  
See these operations as matrix multiplications. The first operation in the code will be the last applied: in the following example, the object is first translated then rotated
```
rotate([0,0,90])
translate([0,0,1])
cube([1,1,2]);
```
- *To keep the cube the same dimension with minkowski*  
Translate it of the ray of added sphere  
Reduce cube size by 2 * the ray 

- *Loops*  
Use for loops to generate a fixed number of elements. I used it to generate a varaiable number of Pins: 
```
// this is executed NbOfPins times
for(i = [0 : NbOfPin-1]){  
            rotate([0,0,360/NbOfPin*i])
            translate([0,-PinSpacer/2, -0.1 *  PinH]) 
            cube([2*ConeOut,PinSpacer, 2*PinH ]); 
        }
```

- *Animate*   
To animate your objects, first activate animation in **View**>**Animation**

Then use `$t` in your code, it ranges from 0 to 1.  
Here is an example with 4 phases, the handle goes down, the support rotate conter clockwise, then clockwise then stop if time is left: 
```
if (0 <= $t && $t <0.2){
    translate([0,0,6*(0.2-$t)/0.2])
    Handle();
    color([0.0,0.5,0.5]) FullSupport();
}

else if (0.2 <= $t && $t < 0.6){
    Handle();
    color([0.0,0.5,0.5]) rotate([0,0,($t-0.2)*180/0.4]) FullSupport();
}
else if (0.6 <= $t && $t< 1){
    Handle();
    color([0.0,0.5,0.5]) rotate([0,0,180-180/0.4*($t-0.6)]) FullSupport();
}
else{
    Handle();
    color([0.0,0.5,0.5]) FullSupport();
}
```
- *Export the animation as a serie of png images*
Select **Dump Pictures** at the right of the steps filling box. The images will be downloaded in the folder where the .SCAD file is.  
Use these to make a gif as the one above.



## Rework the edges with freeCAD
To help the pins to slide in the hole, some rework of the edges is advisable.  
I thought of 2 ways to do so, either remove a cone from the handle or use freeCAD to do it for us.  
I used the second method to get a glimpse on how freeCAD works.  
Below is a step by step method on "How to rework edges with freeCAD". The illustrations are related to these steps.


### Open the .scad file with freeCAD
![](images/Module2Im/PartAfreeCAD.png)


1. Download freeCAD and open it: [freeCAD downloads](https://www.freecad.org/downloads.php?lang=fr)
2. Select the start with arrow button, then openSCAD, then open your scad file (part1 here)
3. Click on "Part" and select "Part Design"
4. Select difference
5. Click on the orange Sheep to create a clone you can work on
6. Unselect difference by **pressing space bar** while clicking on it

### Remork the edges of the clone
![](images/Module2Im/PartBfreeCAD.png)
7. Click on "Part Design" and select "Part"
8. Click on clone 
9. Click on the "Chamfer" icon
10. Select the edges to rework with the "left click + CTRL"
11. Choose the cutting depth you want
12. Press OK
13. (if necessary) unselect the clone, select the Chamfer
You now have got your 3D modeled handle with reworked edges in .FCStd format

### Tricks
- *Change the navigation style*  
To change the navigation style to per example the openSCAD version:  
**right click** the display > **navigation style** > openSCAD

### Other method using openSCAD
I substracted a cylinder at the bottom of the hole of the handle(top part). 

This is the obtained result which is as good as with freeCAD (don't bother using freeCAD when not necessary !): 
![](images/Module2Im/handlechamfer.png)


## Other shared projects
### The rain meter box

This 3D box was made for another projects with the tools I learned in this course and I am sharing it with whom might need something similar. 

There are 2 empty spaces (big engough for a PSOC) and 3 open traps. 1 for a glass window that senses rain and 2 to access the inside. These 2 bottom traps can be closed with traps and 3mm screw that come with the CAD file.  
For printing, you should place the top layer on the floor and print it upside down.
![](images/Module2Im/boiteRain.png)  
You can download the CAD file from the files/module2and3/boiteRain.cad of my gitlab repository and use it for non commercial purpose.
## Licences 
To allow others to use and contribute to your work, whatever you are sharing, you must explicitly tell which license your are using. Otherwise copyright applies.  
The license I use here is *Creative Commons license*. There are different flavors of it which are described here: [https://creativecommons.org/licenses/](https://creativecommons.org/licenses/)

- CC BY
- CC BY-SA
- CC BY-NC
- CC BY-NC-SA
- CC BY-ND
- CC BY-NC-ND

Here is a sample comment to use in the beginning of our code to license it: 
``` 
// File : handleAnimated.scad

// Author : Antoine Trillet

// Date : 27 february 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```
