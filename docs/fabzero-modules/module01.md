# 1. Project managment and documentation

This first week, we have been confronted with new ways to use our computer, using the terminal and learned how to document our work on a website. This website is updated regularly from a gitlab repository using mkdoc. Changing the markdown (.md) files in the repository online will eventually change the content of the website.


## My workload managment method
### The "Get done with it" method

The major factor driving my methodology is the need to save free time to do my master thesis. To do so I try to always finish the week homework and documentation within the day or the next day.  
It is really hard to keep up but it gives me a clear deadline and challenges me not to procrastinate.  

## Using the terminal
Here is a Github page that gathers some useful commands you might need while using the terminal this session or later : [Bash Cheat Sheet](https://github.com/RehanSaeed/Bash-Cheat-Sheet)

## Using GitLab
In order to modify our website and document our work and difficulties, we are going to use GitLab.
A repository has been created by the organizors for each student who can modify it directly online. 
  
The GitLab repository of the course and these websites are available from the course site: [Course Home Page](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/#ulb-how-to-make-almost-any-experiments-fabzero-inside ) 


## Setting up Git locally

To ease the documentation process, it is advised to work on a local version of the repository. To do so we are going to use Git to clone the project locally, commit changes (a bit like saving the files state and the modifications history) and finally push on GitLab all the commits that were done locally.


### Download Git 

First things first, let's download Git : [Git download](https://git-scm.com/downloads)

### Use the GitBash terminal on Windows

I am on Windows, I will explain you how to configure an SSH connection with GitLab using the Git Bash terminal that is included in the Windows download of Git.  
*You can also use your prefered terminal but this one already includes everything will need for Git*

Create a file directory where you want to copy the project, right click on an empty space and select open git bash.  

![](images/Module1Im/OpenGitBash.png)

*Note: that you can also open a git GUI(graphical user interface), but using git with the terminal is definitely better. It is faster once you get used to it and it is much more documented.*

*Paste on GitBash: MiddleMouse click (can be changed in "rightclick --> option --> mouse")*
### Create an SSH key connection
The reason for using an SSH connection rather than an HTTPs is to reduce how often you must enter your password and encrypt the connection, making it more secure. However HTTPs connections are simpler to use. 

First, configure you Git email and username so it matches the ones on GitLab:   
![](images/Module1Im/ConfigGit.png)

Now, it is time to create an SSH key pair. You should have 6.5 or any later version, check it first with *ssh -V*, previous versions are no longer as secure.  

You will be asked to choose where to save the key, press enter to select the default location, otherwise you will have to point the the location of your key later.  

You must also select a passphrase, similar to a password, press enter if you want none, and don't want to reenter it later.   

![](images/Module1Im/SSH.png)

Two keys have been generated and saved at a chosen location, the private key - **Never share your private key** - that is used to encrypt information before sending them over the internet and the public key (that we will add on GitLab) that is used to understand what has been encrypted.

To easily recover this public key, you can clip it from the terminal(with .pub for the public key):   
```
$ cat ~/.ssh/id_ed25519.pub | clip
```

Add this key to your GitLab profile: [Gitlab Profile Key](https://gitlab.com/-/profile/keys)

Check if the connection is well established with the following commands:   
![](images/Module1Im/CheckConn.png)

### Clone the repository
Now that you have established a secure connection with gitlab, you can clone the repository locally.  
On the gitlab page of your repository, copy the "clone with SSH" link:   
![](images/Module1Im/copylinkSSH.png)

Verify that you are in the directory where you want to clone your repository.  
*Trick: You can move toward a specific directory using:*
```
$ cd "path/to/directory"
```
Then clone the repository using `git clone "link"`  

![](images/Module1Im/clone.png)

Now you can modify the files locally, add and commit the changes and push 1 or more commits online. No file will be changed online as long as you don't commit and push. 

|Command       |  Description    |
|--------------|-----------------|
|  `git pull`  | update the local repository from the online version    |
|`git add -A`| add all new files to the staged files| 
|`git commit -am "message"`| add and commit all changes with a commit message|
|`git push`| send commits to the server|

*Troubleshooting: If you have like me tried to commit without message on gitbash, you must have been send to this "page". The only thing I can help you with is to tell you how to leave: Press `ctrl+C` then `:qa!`*  
![](images/Module1Im/commita.png)


## Using Visual Studio Code as an editor for MarkDown

To help us dowument and write in markdown, it is recommended to use a nice editor. I personnaly went for Visual Studio Code (VSC), as I have it installed and it also handles commiting, pushing and merging graphically, which I prefer for the sake of my eyes.

Here is a link to download VSC: [Visual Studio](https://code.visualstudio.com/)

There exist some extensions for markdown that you can install as well. I did not go that road yet. 

One useful command though is `Ctrl+ Shift + V` to display a preview of the files:   

![](images/Module1Im/Preview.png)

*Side Note: if you click on the third icon on the left (source control) you can add files, commit changes and push direclty from VSC.  
Alternatively you can open a terminal from VSC with `Ctrl + ù`*

### Useful VSC shortcuts
|Command       |  Description    |
|--------------|-----------------|
|  `Alt` + `Shift`+ `Copy`  | Select than copy a collumn    |
|`Alt`+ `Shift`+ `Paste`| Select same number of rows and paste a collumn| 
|`Ctrl` + `Shift` + `V`| Preview markdown|
|`Ctrl` + `E`| Open search bar|
|`Windows` + `V`| Open all copied texts and images|
|`Ctrl` + `Shift` + `P`|Open Command palette|
|`Ctrl`+`ù`|Open terminal|

## Markdown (.md) files and syntax
Markdown files are useful for us because they allow for a quickly obtained good looking result. It is also quite exportable between platforms and can be easily translated to htlm. This is what the server does to display our web page from our repository .md and config files.  
Yet, to write markdown files efficiently, one need to familiarize with its syntax. Here is a site that summarize the basics: [Markdown Cheat Cheat](https://www.markdownguide.org/cheat-sheet/) or this one [Github cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Install bash on Windows
1. Developper mode: **Settings**> **Update and security**> **For developpers** > **Enable Developper mode**
2. Install WSL windows subsystem for linux: open **Powershell** as administrator > write the following command and **reboot** : 
```
wsl --install
```
3. To set up your linux account: open **Ubuntu** > wait for installation to finish + **press enter** > choose a **username and a password** (*nothing will appear on screen it is normal*)
4. You are good to go, you can open bash from search tab anytime.  
More information and tricks on : [set-up-your-linux-username-and-password](https://learn.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password)

5. To access windows files: C:\ becomes /mnt/c/  
Or alternatively: `Shift + right click` in folder > open powershell window> type `bash`

## Imagemagick : GIFs, Compression,...

### Make GIFs out of Bash
1. Open Bash
2. Make sure your ubuntu version is updated with 
```
sudo apt-get update
```
3. Write the following command and accept(Y) (*~130Mb of disk required*)
```
sudo apt-get install imagemagick
```
4. Go to directory with the pictures and write
```
convert -delay 100 -loop 0 *.png animatedGIF.gif
``` 
100 refers to the time in ms between images

### Make a mosaic of images
Open bash in the folder containing the images and write down: 
```
montage *.jpg mosaic.jpg
```
To enable greater and custom resolution (+2 for 2 pixel gaps between pics):  
```
montage -geometry 1024x1024+2+2 *.jpg mosaic.jpg
```
For more options check this blog post: [montage with image magick](https://sinestesia.co/blog/tutorials/quick-n-easy-mosaics-with-imagemagick/)
### Convert images between formats
Open bash in the directory where the image is and type
```
convert images.jpg -resize 50% -rotate 90 image.png
```
Learn more about convert: [convert imagemagick](https://doc.ubuntu-fr.org/imagemagick)
### Compress images in a folder

Use this command in Bash: 
```
convert -strip -interlace Plane -gaussian-blur 0.05 -quality 85% source.jpg result.jpg
```
Where a gaussian blur is added to reduce the size after comrpession.  
Quality is kept at 85% which will help compressing more.  
Soure.jpg is the source file, result.jpg the result name, use *.jpg to do it on all files in the folder.

## ffmpeg for mp4 files
### Extract a part from a video
1. Download ffmpeg with 
```
$ sudo apt install ffmpeg
```
2. Extract a specific duration (10 min) from the start(5:20 min) with this command: 
```
$ ffmpeg -i input.mp4 -ss 00:05:20 -t 00:10:00 -c:v copy -c:a copy output1.mp4
```
### Convert video to gif
```
ffmpeg -i output.mp4 -vf "fps=5" -loop 0 output2.gif
```
### Scale video size

```
 ffmpeg -i outputResult.mp4 -vf scale=400:-1 output.mp4
```
-1 ensure we keep the same aspect ratio



## Some other useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Issues to visualise advancement in the documentation
A csv file is available in the 6th module "Evaluation" with all issues.  
Here is a link to import the .csv file into the issue  page of your gitlab course page (issue -> List -> import issue): [import csv](https://docs.gitlab.com/ee/user/project/issues/csv_import.html)

Then to create a board that eases the issue managment, click on **issues->board->development-> create new board ->(don't show lists closed /open) -> create list-> then select a label**  
You will get something like this:  
![](images/Module1Im/IssueBoard.png)

To evaluate my pair documentation, I am using a template given by the professor. I simply go to my pair board and if an assignement is done(green), I can open it and *write* below. Here is the template:  
```
## Cross-evaluation form - (Write here your evaluator name)

1: strongly disagree  
5: strongly agree

Is the documentation for this module:

**Structured** [1-2-3-4-5]

* Is the structure understood at first glance (quick navigation)?
* Are the purpose, accomplishment and process clearly presented and structured (using headings, code images, sections,...)?

(Comments)

**Complete** [1-2-3-4-5]

* Are the exercise objectives and checklist completed?
* Is the documentation complete? Do the hyperlinks work? Are the files and source codes accessible? Are the machine parameters and references of the materials used provided?

(Comments)

**Honest** [1-2-3-4-5]

* Are sources and inspirations properly referenced? Are the licenses used correctly?
* Is it clear what works, what doesn't work and what needs to be improved?

(Comments)

**pedagogical/appropriate** \[1-2-3-4-5\]

* Based on the documentation, is the work reproducible if one has access to a fablab?
* Is there a pedagogical effort to make the documentation accessible and appropriate (style, clarity of diagrams, comments in the codes,...) ?

(Comments)

(general comment)
```



