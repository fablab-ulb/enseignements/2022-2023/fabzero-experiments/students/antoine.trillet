# 1. Project managment and documentation

This first week, we have been confronted with new ways to use our computer, using the terminal and learned how to document our work on a website. This website is updated regularly from a gitlab repository using mkdoc. Changing the markdown (.md) files in the repository online will eventually change the content of the website.

## Using the terminal
Here is a Github page that gathers some useful commands you might need while using the terminal this session or later : https://github.com/RehanSaeed/Bash-Cheat-Sheet

## Using GitLab
In order to modify our website and document our work and difficulties, we are going to use GitLab.
A repository has been created by the organizors for each student who can modify it directly online. 
  
The GitLab repository of the course and these websites are available from the course site: [fablab-ulb-gitlab](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/#ulb-how-to-make-almost-any-experiments-fabzero-inside ) 


## Setting up Git locally

To ease the documentation process, it is advised to work on a local version of the repository. To do so we are going to use Git to clone the project locally, commit changes (a bit like saving the files state and the modifications history) and finally push on GitLab all the commits that were done locally.


### Download Git 

First things first, let's download Git : https://git-scm.com/downloads

### Use the GitBash terminal on Windows

I am on Windows, I will explain you how to configure an SSH connection with GitLab using the Git Bash terminal that is included in the Windows download of Git.  
*You can also use your prefered terminal but this one already includes everything will need for Git*

Create a file directory where you want to copy the project, right click on an empty space and select open git bash.

![](images/Module1Im/OpenGitBash.png)

*Note: that you can also open a git GUI(graphical user interface), but using git with the terminal is definitely better. It is faster once you get used to it and it is much more documented.*

*Paste on GitBash: MiddleMouse click (can be changed in "rightclick --> option --> mouse")*
### Create an SSH key connection
The reason for using an SSH connection rather than an HTTPs is to reduce how often you must enter your password and encrypt the connection, making it more secure. However HTTPs connections are simpler to use. 

First, configure you Git email and username so it matches the ones on GitLab: 
![](images/Module1Im/ConfigGit.png)

Now, it is time to create an SSH key pair. You should have 6.5 or any later version, check it first with *ssh -V*, previous versions are no longer as secure.  

You will be asked to choose where to save the key, press enter to select the default location, otherwise you will have to point the the location of your key later.  

You must also select a passphrase, similar to a password, press enter if you want none, and don't want to reenter it later. 

![](images/Module1Im/SSH.png)

Two keys have been generated and saved at a chosen location, the private key - **Never share your private key** - that is used to encrypt information before sending them over the internet and the public key (that we will add on GitLab) that is used to understand what has been encrypted.

To easily recover this public key, you can clip it from the terminal(with .pub for the public key): 
![](images/Module1Im/pubclip.png)

Add this key to your GitLab profile: https://gitlab.com/-/profile/keys

Check if the connection is well established with the following commands: 
![](images/Module1Im/CheckConn.png)

### Clone the repository
Now that you have established a secure connection with gitlab, you can clone the repository locally.  
On the gitlab page of your repository, copy the "clone with SSH" link: 
![](images/Module1Im/copylinkSSH.png)

Verify that you are in the directory where you want to clone your repository.  
*Trick: You can move toward a specific directory using:*
```
$ cd "path/to/directory"
```
Then clone the repository using `git clone "link"`

![](images/Module1Im/clone.png)

Now you can modify the files locally, add and commit the changes and push 1 or more commits online. No file will be changed online as long as you don't commit and push. 

|Command       |  Description    |
|--------------|-----------------|
|  `git pull`  | update the local repository from the online version    |
|`git add -A`| add all new files to the staged files| 
|`git commit -am "message"`| add and commit all changes with a commit message|
|`git push`| send commits to the server|

*Troubleshooting: If you have like me tried to commit without message on gitbash, you must have been send to this "page". The only thing I can help you with is to tell you how to leave: Press `ctrl+C` then `:qa!`*
![](images/Module1Im/commita.png)


## Using Visual Studio Code as an editor for MarkDown

To help us dowument and write in markdown, it is recommended to use a nice editor. I personnaly went for Visual Studio Code (VSC), as I have it installed and it also handles commiting, pushing and merging graphically, which I prefer for the sake of my eyes.

Here is a link to download VSC: https://code.visualstudio.com/

There exist some extensions for markdown that you can install as well. I did not go that road yet. 

One useful command though is `Ctrl+ Shift + V` to display a preview of the files: 

![](images/Module1Im/Preview.png)

*Side Note: if you click on the third icon on the left (source control) you can add files, commit changes and push direclty from VSC.  
Alternatively you can open a terminal from VSC with `Ctrl + Shift + ù`*

## Markdown (.md) files and syntax
Markdown files are useful for us because they allow for a quickly obtained good looking result. It is also quite exportable between platforms and can be easily translated to htlm. This is what the server does to display our web page from our repository .md and config files.  
Yet, to write markdown files efficiently, one need to familiarize with its syntax. Here is a site that summarize the basics: https://www.markdownguide.org/cheat-sheet/ 

## Some other useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
